//
//  LoadingView.m
//  bunqAssignment
//
//  Created by Shahriyar So on 13/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

/**
 This method returns a LoadingView object and presents the loadingView
 on the superView. Pass the view you want the loading to appear on top of it as superView arguement. Set the title to be rendered as loading title.
 */
+ (LoadingView *)startLoading:(UIView *)superView title:(NSString*)title
{
    LoadingView *loadingView = [[LoadingView alloc] initWithFrame:superView.bounds];
    
    // If something's gone wrong.
    if (!loadingView) { return nil; }
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleLarge];
    [indicator setColor:[UIColor whiteColor]];
    indicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    UILabel* label = [[UILabel alloc] init];
    [label setText:title];
    [label setTextColor:[UIColor whiteColor]];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    
    [loadingView addSubview:label];
    [loadingView addSubview:indicator];
    // Indicator constraints
    [indicator.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicator.superview attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:indicator attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [indicator.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicator.superview attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:indicator attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    // Label constraints
    [label.superview addConstraint:[NSLayoutConstraint constraintWithItem:label.superview attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:label attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [label.superview addConstraint:[NSLayoutConstraint constraintWithItem:label.superview attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:label attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:50]];

    [indicator startAnimating];
    
    [superView addSubview:loadingView];
    
    return loadingView;
}

/// Remove the LoadingView object from the superView.
- (void)stopLoading
{
    [super removeFromSuperview];
}


@end
