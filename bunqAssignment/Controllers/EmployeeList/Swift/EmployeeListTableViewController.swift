//
//  EmployeeListTableViewController.swift
//  bunqAssignment
//
//  Created by Shahriyar So on 15/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

import UIKit

class EmployeeListTableViewController: UITableViewController
{
    private var employeeList: [Employee]?
    private lazy var employeeDir: EmployeeDirectory = EmployeeDirectory()
    private weak var loadingView: LoadingView?
    private let cellID = "employeeCell"
    private var isAscending = false;
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Private methods
    
    fileprivate func initialSetup() {
        self.setupSortButton()
        
        NotificationCenter.default.addObserver(self, selector: #selector(employeesUpdated), name: NSNotification.Name.employeeDirectoryDidUpdate, object: nil)
        
        navigationItem.title = "Employee salary list".localize()
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.register(EmployeeInfoCell.self, forCellReuseIdentifier: self.cellID)
        tableView.allowsSelection = false
        
        self.loadingView = LoadingView.startLoading(view, title: "Retriving data...".localize())
        self.employeeDir.update()
    }
    
    fileprivate func setupSortButton() {
        let sortButton = UIBarButtonItem(title: "A-Z", style: .done, target: self, action: #selector(sortListByName(_:)))
        
        self.navigationItem.rightBarButtonItem = sortButton;
    }
    
    // MARK: Actions
    
    @objc private func employeesUpdated(_ notification: Notification) {
        self.employeeList = self.employeeDir.employees
        
        self.tableView.reloadData()
        
        self.loadingView?.stopLoading()
    }
    
    @objc private func sortListByName(_ sender: UIBarButtonItem) {
        guard self.employeeList != nil else { return } // Not unwrapping here because of in-place sort.
        
        self.isAscending.toggle()
        sender.title = self.isAscending ? "Z-A" : "A-Z"
        
        self.loadingView = LoadingView.startLoading(view, title: "Sorting...".localize())
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            
            Thread.sleep(forTimeInterval: 0.7)
            
            self.isAscending ? (self.employeeList?.sort(by: { $0.name < $1.name })) : (self.employeeList?.sort(by: { $0.name > $1.name }))
            
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
                // Scroll to top
                let indexPathFirst = IndexPath(row: 0, section: 0)
                self?.tableView.scrollToRow(at: indexPathFirst, at: .top, animated: false)
                
                self?.loadingView?.stopLoading()
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.employeeList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! EmployeeInfoCell
        guard let employeeList = self.employeeList else { return cell }
        
        let imageNames = ["face-mask", "face-mask_female"]
        let employee   = employeeList[indexPath.row]
        let image      = UIImage(named: imageNames[indexPath.row % 2])
        
        // Color blending fix
        cell.employeeBirthdayLabel.backgroundColor = tableView.backgroundColor
        cell.employeeSalaryLabel.backgroundColor = tableView.backgroundColor
        cell.employeeNameLabel.backgroundColor = tableView.backgroundColor
        
        // Set cell data
        cell.employeeBirthdayLabel.text = String(format: "Birth year: %tu", employee.birthYear)
        cell.employeeSalaryLabel.text = employee.getFormattedSalary()
        cell.employeeNameLabel.text = employee.name
        cell.employeePhoto.image = image

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
