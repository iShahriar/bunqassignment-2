//
//  EmployeeListViewController.h
//  bunqAssignment
//
//  Created by Shahriyar So on 12/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#ifndef EmployeeListViewController_h
#define EmployeeListViewController_h

#import <UIKit/UIKit.h>

@interface EmployeeListViewController : UITableViewController

@end

#endif /* EmployeeListViewController_h */


