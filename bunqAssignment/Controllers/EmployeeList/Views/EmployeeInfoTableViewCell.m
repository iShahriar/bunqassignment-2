//
//  EmployeeInfoTableViewCell.m
//  bunqAssignment
//
//  Created by Shahriyar So on 14/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmployeeInfoTableViewCell.h"

@implementation EmployeeInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
    if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier])
    {
        [self setupEmployeeImageView];
        [self setupSalaryLabel];
        [self setupEmployeeNameAndBirthdayLabel];
    }
    
    return self;
}

#pragma mark - Privates

- (void)setupEmployeeImageView
{
    if (!_employeePhoto)
    {
        _employeePhoto = [[UIImageView alloc] initWithFrame:CGRectZero];
        _employeePhoto.contentMode = UIViewContentModeScaleAspectFit;
        _employeePhoto.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:_employeePhoto];
        
        [NSLayoutConstraint activateConstraints:@[
            [_employeePhoto.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:16],
            [_employeePhoto.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor],
            [_employeePhoto.heightAnchor constraintEqualToConstant:50],
            [_employeePhoto.widthAnchor constraintEqualToConstant:50]
        ]];
    }
}

- (void)setupEmployeeNameAndBirthdayLabel
{
    if (!_employeeNameLabel && !_employeeBirthdayLabel)
    {
        _employeeNameLabel = [[UILabel alloc] init];
        _employeeBirthdayLabel = [[UILabel alloc] init];
        
        [_employeeBirthdayLabel setFont:[UIFont systemFontOfSize:12]];
        [_employeeNameLabel setFont:[UIFont boldSystemFontOfSize:13]];
        
        UIStackView* stackView = [[UIStackView alloc] initWithArrangedSubviews:@[_employeeNameLabel, _employeeBirthdayLabel]];
        [stackView setAxis:UILayoutConstraintAxisVertical];
        [stackView setDistribution:UIStackViewDistributionFill];
        [stackView setSpacing:8];
        stackView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.contentView addSubview:stackView];
        
        [NSLayoutConstraint activateConstraints:@[
            [stackView.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor],
            [stackView.leadingAnchor constraintEqualToAnchor:_employeePhoto.trailingAnchor constant:13],
            [stackView.trailingAnchor constraintLessThanOrEqualToAnchor:_emplyeeSalaryLabel.leadingAnchor constant:-15]
        ]];
    }
}

- (void)setupSalaryLabel
{
    if (!_emplyeeSalaryLabel)
    {
        _emplyeeSalaryLabel = [[UILabel alloc] init];
        _emplyeeSalaryLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_emplyeeSalaryLabel setFont: [UIFont boldSystemFontOfSize:17]];
        
        [self.contentView addSubview:_emplyeeSalaryLabel];
        
        [NSLayoutConstraint activateConstraints:@[
            [_emplyeeSalaryLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor],
            [_emplyeeSalaryLabel.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-16],
        ]];
    }
}

@end
