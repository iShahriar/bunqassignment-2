//
//  EmployeeInfoCell.swift
//  bunqAssignment
//
//  Created by Shahriyar So on 15/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

import UIKit

final class EmployeeInfoCell: UITableViewCell
{

    // MARK: Views
    
    lazy var employeePhoto: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false

        return iv
    }()
    
    lazy var employeeNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 13)

        return lbl
    }()
    
    lazy var employeeSalaryLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        
        return lbl
    }()
    
    lazy var employeeBirthdayLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        
        return lbl
    }()
    
    lazy var stackView: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [self.employeeNameLabel, self.employeeBirthdayLabel])
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = .vertical
        sv.distribution = .fill
        sv.spacing = 8
        
        return sv
    }()
    
    // MARK: Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupLayouts()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private
    
    fileprivate func setupLayouts() {
        contentView.addSubviews(self.stackView, self.employeePhoto, self.employeeSalaryLabel)
        
        NSLayoutConstraint.activate([
            self.employeePhoto.heightAnchor.constraint(equalToConstant: 50),
            self.employeePhoto.widthAnchor.constraint(equalToConstant: 50),
            self.employeePhoto.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            self.employeePhoto.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            
            self.employeeSalaryLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            self.employeeSalaryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            
            self.stackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            self.stackView.leadingAnchor.constraint(equalTo: self.employeePhoto.trailingAnchor, constant: 3),
            self.stackView.trailingAnchor.constraint(lessThanOrEqualTo: self.employeeSalaryLabel.leadingAnchor, constant: -15),
        ])
    }
    
}
