//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//
#ifndef bunqAssignment_Bridging_Header_h
#define bunqAssignment_Bridging_Header_h

#import "Employee.h"
#import "EmployeeDirectory.h"
#import "LoadingView.h"

#endif
