//
//  EmployeeDirectory.h
//  bunqAssignment
//
//  Created by Shahriyar So on 13/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#ifndef EmployeeDirectory_h
#define EmployeeDirectory_h

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class Employee;

// notification posted when the directory finishes updating
extern NSString* _Nonnull const kEmployeeDirectoryDidUpdateNotification;

@interface EmployeeDirectory : NSObject

@property (readonly, nullable) NSArray<Employee*>* employees;
@property (readonly) BOOL isUpdating;

- (void)update;

@end

NS_ASSUME_NONNULL_END

#endif /* EmployeeDirectory_h */


