//
//  LoadingView.h
//  bunqAssignment
//
//  Created by Shahriyar So on 13/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoadingView: UIView

+ (LoadingView *) startLoading:(UIView *) superView title:(NSString*)title;
- (void) stopLoading;

@end

NS_ASSUME_NONNULL_END
