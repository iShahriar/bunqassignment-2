//
//  AppDelegate.h
//  bunqAssignment
//
//  Created by Shahriyar So on 12/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

