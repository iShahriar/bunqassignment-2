//
//  String+Extensions.swift
//  bunqAssignment
//
//  Created by Shahriyar So on 15/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

import Foundation

extension String
{
    /**
     Wrapper around NSLocalizedString for less code.
     - Parameter comment: Localization comment. default "".
     - Description: "Your String".localize()
     - Returns: Localized String.
     */
    func localize(_ comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
