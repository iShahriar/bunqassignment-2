//
//  EmployeeInfoTableViewCell.h
//  bunqAssignment
//
//  Created by Shahriyar So on 14/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeInfoTableViewCell : UITableViewCell

@property (nonatomic) UIImageView* employeePhoto;
@property (nonatomic) UILabel* employeeNameLabel;
@property (nonatomic) UILabel* employeeBirthdayLabel;
@property (nonatomic) UILabel* emplyeeSalaryLabel;

@end

NS_ASSUME_NONNULL_END
