//
//  SceneDelegate.h
//  bunqAssignment
//
//  Created by Shahriyar So on 12/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

