//
//  UIView+Extensions.swift
//  bunqAssignment
//
//  Created by Shahriyar So on 15/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

import Foundation

extension UIView
{
    /**
     Add Subviews
     - Parameters views: Views you want to add separated by comma.
     */
    func addSubviews(_ views: UIView...) {
        views.forEach({ addSubview($0) })
    }
}
