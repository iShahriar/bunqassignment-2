//
//  Employee.m
//  bunqAssignment
//
//  Created by Shahriyar So on 13/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"

NS_ASSUME_NONNULL_BEGIN

static NSUInteger const kStartingSalary = 10000;
NSString* const kSalaryCurrency = @"EUR";

@implementation Employee

- (instancetype)initWithName:(NSString *)name birthYear:(NSUInteger)birthYear
{
    self = [super init];
    
    if (self)
    {
        _name = [name copy];
        _birthYear = birthYear;
        _salary = [[NSDecimalNumber  alloc] initWithUnsignedInteger:kStartingSalary];
    }
    
    return self;
}

#pragma mark - Class methods
// Method could take an arguement to format the currency based on currency code.
// But i'm not sure if it's what the assignment asks, as we have a const kSalaryCurrency.
- (NSString*)getFormattedSalary
{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
    formatter.currencyCode = kSalaryCurrency;
    formatter.usesGroupingSeparator = YES;
    formatter.currencyDecimalSeparator = @","; // Based on Europe currency format xx.xxx,xx
    formatter.currencyGroupingSeparator = @".";
    
    return [formatter stringFromNumber:self.salary];
}

@end

NS_ASSUME_NONNULL_END
