//
//  Employee.h
//  bunqAssignment
//
//  Created by Shahriyar So on 13/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#ifndef Employee_h
#define Employee_h


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString* const kSalaryCurrency;

@interface Employee : NSObject

@property (readonly, copy) NSString* name;
@property (readonly) NSUInteger birthYear;
@property (readonly, copy) NSDecimalNumber* salary;

- (instancetype)initWithName:(NSString*)name birthYear:(NSUInteger)birthYear;
- (NSString*)getFormattedSalary;

@end

NS_ASSUME_NONNULL_END

#endif /* Employee_h */

