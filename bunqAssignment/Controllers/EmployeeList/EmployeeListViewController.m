//
//  EmployeeListViewController.m
//  bunqAssignment
//
//  Created by Shahriyar So on 12/06/2020.
//  Copyright © 2020 Shahriar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmployeeListViewController.h"
#import "Employee.h"
#import "EmployeeDirectory.h"
#import "LoadingView.h"
#import "EmployeeInfoTableViewCell.h"

@interface EmployeeListViewController ()

@property (strong, nonatomic) NSMutableArray<Employee*> *employeeList;
@property (strong, nonatomic) EmployeeDirectory* employeeDir;
@property (weak, nonatomic) LoadingView* loadingView;
@property (nonatomic) BOOL isAscending;

@end

static NSString* const cellId = @"employeeCell";

@implementation EmployeeListViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialSetup];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.loadingView = [LoadingView startLoading:self.view
                                           title:NSLocalizedString(@"Loading...", @"")]; // Start the loading
    
    [self.employeeDir update];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Privates

- (void)initialSetup
{
    [self setupSortButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(employeesUpdated:)
                                                 name:kEmployeeDirectoryDidUpdateNotification
                                               object:nil];
    
    self.navigationItem.title = NSLocalizedString(@"Employee salary list", "");
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    
    [self.tableView registerClass: EmployeeInfoTableViewCell.class forCellReuseIdentifier:cellId];
    self.tableView.allowsSelection = NO;
    
    self.employeeDir = [[EmployeeDirectory alloc] init];
}

- (void)employeesUpdated:(NSNotification *) notification
{
    
    self.employeeList = [NSMutableArray arrayWithArray:self.employeeDir.employees];
    
    [self.tableView reloadData];
    
    [self.loadingView stopLoading]; // Remove the loading when data has been fetched.
}

- (void)setupSortButton
{
    UIBarButtonItem* sortButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"A-Z"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(sortListByName:)];
    
    self.navigationItem.rightBarButtonItem = sortButton;
}

#pragma mark - Actions

- (void)sortListByName:(UIBarButtonItem *)sender
{
    self.isAscending = !self.isAscending;
    self.loadingView = [LoadingView startLoading:self.view
                                           title:NSLocalizedString(@"Sorting...", @"")]; // Start the loading
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSThread sleepForTimeInterval: 0.7];
        
        NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:self.isAscending];
        
        self.employeeList = [NSMutableArray arrayWithArray:[self.employeeList sortedArrayUsingDescriptors:@[sort]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            // Scroll top top when sorting.
            NSIndexPath *indexPathFirst = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView scrollToRowAtIndexPath:indexPathFirst
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:NO];
            
            [self.loadingView stopLoading]; // Remove the loading when data has been fetched.
            [sender setTitle:self.isAscending ? @"Z-A" : @"A-Z"];
        });
    });
}

#pragma mark - UITableView Protocol stubs

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.employeeList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmployeeInfoTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    NSArray<NSString*>* images = @[@"face-mask", @"face-mask_female"];
    UIImage* image = [UIImage imageNamed:images[indexPath.row % 2]];
    Employee* employee  = [self.employeeList objectAtIndex:indexPath.row];
    NSString* formattedSalary = [employee getFormattedSalary];
    
    // Birthday label attributes
    NSDictionary* baseAttributes = @{
        NSForegroundColorAttributeName:[UIColor lightGrayColor],
        NSFontAttributeName:[UIFont systemFontOfSize:12]
    };
    NSDictionary *yearAttribute = @{
        NSFontAttributeName:[UIFont boldSystemFontOfSize:12]
    };
    NSString* birthYear = [NSString stringWithFormat:NSLocalizedString(@"Birth year: %tu", ""), employee.birthYear];
    NSRange birthYearRange = [birthYear rangeOfString:NSLocalizedString(@"Birth year:", "")];
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:birthYear attributes:yearAttribute];
    [attrString addAttributes:baseAttributes range:birthYearRange];
    
    // Set cell data
    [cell.employeePhoto setImage:image];
    [cell.employeeNameLabel setText:employee.name];
    [cell.emplyeeSalaryLabel setText:formattedSalary];
    [cell.employeeBirthdayLabel setAttributedText:attrString];
    
    // Color blending fix
    [cell.employeeBirthdayLabel setBackgroundColor:tableView.backgroundColor];
    [cell.employeeNameLabel setBackgroundColor:tableView.backgroundColor];
    [cell.emplyeeSalaryLabel setBackgroundColor:tableView.backgroundColor];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return UIView.new;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


@end
